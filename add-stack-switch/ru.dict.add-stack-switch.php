<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2013 XXXXX
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

Dict::Add('RU RU', 'Russian', 'Русский', array(
	// Dictionary entries go here
	'Class:StackSwitch' => 'Стек сетевых коммутаторов',
	'Class:StackSwitch/Attribute:typecluster' => 'Тип кластера',
	'Class:StackSwitch/Attribute:networkdevice_list' => 'Сетевые устройства',
	'Class:NetworkDevice/Attribute:stackswitch_id' => 'Имя стека',
));
?>
