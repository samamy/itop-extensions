<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2013 XXXXX
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

Dict::Add('RU RU', 'Russian', 'Русский', array(
	// Dictionary entries go here
	'Class:ClusterFirewall' => 'Кластер межсетевых экранов',
	'Class:ClusterFirewall/Attribute:typecluster' => 'Тип кластера',
	'Class:ClusterFirewall/Attribute:firewall_list' => 'Межсетевые экраны',
	'Class:ClusterFirewall/Attribute:connectablecis_list' => 'Устройства',
));
?>
