<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2013 XXXXX
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

Dict::Add('RU RU', 'Russian', 'Русский', array(
	// Dictionary entries go here
	'Class:Firewall' => 'Межсетевой экран',
	'Class:Firewall/Attribute:networkdevicetype_id' => 'Тип устройства',
        'Class:Firewall/Attribute:iosversion_id' => 'Версия IOS',
        'Class:Firewall/Attribute:ram' => 'ОЗУ',
	'Class:Firewall/Attribute:clusterfirewall_id' => 'Имя кластера',
	'Class:Firewall/Attribute:connectablecis_list' => 'Устройства',
));
?>
